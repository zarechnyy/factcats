//
//  Networking.swift
//  FactCats
//
//  Created by Yaroslav Zarechnyy on 1/6/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


struct outPutModel {
    var firstNameModel: String
    var lastNameModel: String
    var information: String
}

//MARK:- Delegate
protocol networkingDelegate: class {
    func updateFacts(factsJSON: JSON)
}

class Networking {
    
    weak var delegate: networkingDelegate!
    
    func getFactsJSON() {
        request("https://cat-fact.herokuapp.com/facts").responseJSON { (json) in
            let catFactsJSON = JSON(json.data!)
            DispatchQueue.main.async {
                self.delegate.updateFacts(factsJSON: catFactsJSON)
            }
        }
    }
}
