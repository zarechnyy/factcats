//
//  ContentView.swift
//  
//
//  Created by Yaroslav Zarechnyy on 1/6/19.
//

import Foundation
import UIKit
import SwiftyJSON
import MBProgressHUD

class ContentController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    

    @IBAction func logoutBtn(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "loginController")
        self.present(newViewController, animated: true, completion: nil)
    }
    
    //MARK:- Reload to display data
    var network = Networking()
    var fullNameModel = [outPutModel]() {
        didSet{
            DispatchQueue.main.async {
                self.dismissHUD(isAnimated: true)
                self.tableView.reloadData()
            }
        }
    }
    
    func showHUD(progressLabel:String){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = progressLabel
    }
    
    func dismissHUD(isAnimated:Bool) {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showHUD(progressLabel: "Loading...")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK:- Network delegate
        self.network.delegate = self
        self.network.getFactsJSON()
    }
    
    
}

//MARK:- Auto-sizing TB cells
extension ContentController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
    }
}

//MARK:- TableViewDataSource
extension ContentController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fullNameModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellToReturn = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! catFactsCell
        cellToReturn.setFullName(firstName: fullNameModel[indexPath.row].firstNameModel, sureName: fullNameModel[indexPath.row].lastNameModel)
        cellToReturn.additionInfo.text = fullNameModel[indexPath.row].information
        cellToReturn.setImage(icon: #imageLiteral(resourceName: "github.png"))
        return cellToReturn
    }
    
    
}

extension ContentController: networkingDelegate {
    
    func updateFacts(factsJSON: JSON) {
        if let all = factsJSON["all"].array{
            for fullName in all {
                let object = outPutModel(firstNameModel: fullName["user"]["name"]["first"].string ?? "Error", lastNameModel: fullName["user"]["name"]["last"].string ?? "Error", information: fullName["text"].string ?? "Error" )
                fullNameModel.append(object)
                }
            }
        }
}
    

