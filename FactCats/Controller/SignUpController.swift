//
//  SingUpController.swift
//  FactCats
//
//  Created by Yaroslav Zarechnyy on 1/7/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit
import Locksmith

class SignUpController: UIViewController {
    
    
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var confirmTF: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == true{
            //user has already logged in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "contentController")
            self.present(newViewController, animated: false, completion: nil)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    

    
    //MARK:- SignUp new user
    @IBAction func signUpBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Error", message: "Email or password is not valid ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        guard let email = usernameTF.text, usernameTF.text != "", isValidEmail(testStr: email) else {
            self.present(alert, animated: true, completion: nil)
             usernameTF.text = ""
            return
        }
        
        guard let password = passwordTF.text, passwordTF.text != "", passwordTF.text == confirmTF.text, password.count>5 else {
             self.present(alert, animated: true, completion: nil)
            return
        }
        do {
     //     try Locksmith.saveData(data: ["login": "\(email)", "password":"\(password)"], forUserAccount: "test")
            try Locksmith.updateData(data: ["login": "\(email)", "password":"\(password)"], forUserAccount: "test")
            UserDefaults.standard.set(true, forKey: "isLoggedIn")
        } catch {
            print("Fail to save data")
        }
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "contentController")
            self.present(newViewController, animated: true, completion: nil)
    }
    
    
}

