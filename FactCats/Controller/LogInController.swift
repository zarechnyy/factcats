//
//  LogInController.swift
//  FactCats
//
//  Created by Yaroslav Zarechnyy on 1/7/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit
import Locksmith

class LoginController: UIViewController {
    
 
    @IBOutlet weak var usernameTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == true{
            //user has already logged in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "contentController")
            self.present(newViewController, animated: false, completion: nil)
        }
    }
    
    
    @IBAction func loginBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Error", message: "Email or password is not correct ", preferredStyle: .alert)
        let alertAcc = UIAlertController(title: "Error", message: "You might have no account yet ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        alertAcc.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        let dic = Locksmith.loadDataForUserAccount(userAccount: "test")
        
        if let userInfo = dic as? [String:Any]{
            guard let pw = userInfo["password"] as? String else {
                self.present(alertAcc,animated: true)
                return
            }
            guard let username = userInfo["login"] as? String else {
                self.present(alertAcc,animated: true)
                return
            }
            if pw == passwordTF.text, username == usernameTF.text {
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "contentController")
                self.present(newViewController, animated: true, completion: nil)
            } else {
                self.present(alert, animated: true)
            }
        }
        
         self.present(alertAcc,animated: true)
        
//        let pw = dic?["password"]! as! String
//        let username = dic?["login"]! as! String
//        print(pw)
//        print(username)
        

    }
    
}
