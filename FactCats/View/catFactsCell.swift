//
//  catFactsCell.swift
//  FactCats
//
//  Created by Yaroslav Zarechnyy on 1/6/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit


class catFactsCell: UITableViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    
    @IBOutlet weak var fullName: UILabel!
    
    @IBOutlet weak var additionInfo: UILabel!
    
    func setImage(icon: UIImage) {
        imageCell.image = icon
    }
    
    func setFullName(firstName: String, sureName: String) {
        fullName.text = "\(firstName) \(sureName)"
    }
}
